module.exports.config = {
  directConnect: true,
  baseUrl: "https://www.avenuecode.com.br",
  specs: ["test/e2e/tests/specs/*.spec.js"],
  capabilities: {
    'browserName': 'chrome',
    },
 
  onPrepare: () => {
    browser.ignoreSynchronization = true;
    browser.get('')
  }
}
