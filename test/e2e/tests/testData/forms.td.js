const faker = require('faker')

class forms {
    constructor() {
        this.randomFirstName = faker.name.firstName()
        this.randomLastName = faker.name.lastName()
        this.randomEmail = faker.internet.exampleEmail()
        this.randomCompany = faker.company.companyName()
        this.randomPhone = faker.phone.phoneNumber('31########')
        this.randomTellAboutProject = faker.lorem.paragraph()
    }
}

module.exports = forms;