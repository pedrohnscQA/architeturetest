const WhatWeDo = require('../pages/whatWeDo/whatWeDo.po')
const StrategyAndConsulting = require('../pages/whatWeDo/strategyAndConsulting.po')
const Forms = require('../testData/forms.td')
const Messages = require('../pages/components/messages')

describe('I should test the behavior of what we do page navigating between pages and submit the form Strategy and Consulting', () => {

    const whatWeDo = new WhatWeDo()
    const strategyAndConsulting = new StrategyAndConsulting()
    const forms = new Forms()
    const messages = new Messages()

    it('Should verify if the user can submit the form of Strategy and Consulting with success', () => {
        whatWeDo.clickLinkWhatWeDo()
        whatWeDo.clickInTheBox()
        strategyAndConsulting.fillName(forms.randomFirstName)
        strategyAndConsulting.fillEmail(forms.randomEmail)
        strategyAndConsulting.chooseLocationOption()
        strategyAndConsulting.fillABoutProject(forms.randomTellAboutProject)
        strategyAndConsulting.submitForm()
        expect(messages.getSuccesMessage()).toContain(messages.formSubmitedSucessMessage)
    })
})

describe('I should test the behaviour of what whe do page with blank input fields', () => {

    const strategyAndConsulting = new StrategyAndConsulting()
    const forms = new Forms()
    const messages = new Messages()

    beforeEach(() => {
        browser.get('/what-we-do/strategy-and-consulting')
    })

    it("Should verify if the user can't submit the form of Strategy and Consulting leaving the name in blank", () => {
        strategyAndConsulting.fillName('')
        strategyAndConsulting.fillEmail(forms.randomEmail)
        strategyAndConsulting.chooseLocationOption()
        strategyAndConsulting.fillABoutProject(forms.randomTellAboutProject)
        strategyAndConsulting.submitForm()
        expect(messages.getNameErrorMessage()).toContain(messages.nameMessage)
    })

    it("Should verify if the user can't submit the form of Strategy and Consulting leaving the email in blank", () => {
        strategyAndConsulting.fillName(forms.randomFirstName)
        strategyAndConsulting.fillEmail('')
        strategyAndConsulting.chooseLocationOption()
        strategyAndConsulting.fillABoutProject(forms.randomTellAboutProject)
        strategyAndConsulting.submitForm()
        expect(messages.getEmailErrorMessage()).toContain(messages.emailMessage)
    })

    it("Should verify if the user can't submit the form of Strategy and Consulting leaving the location in blank", () => {
        strategyAndConsulting.fillName(forms.randomFirstName)
        strategyAndConsulting.fillEmail(forms.randomEmail)
        strategyAndConsulting.fillABoutProject(forms.randomTellAboutProject)
        strategyAndConsulting.submitForm()
        expect(messages.getSelectionLocationErrorMessage()).toContain(messages.selectLocation)
    })

    it("Should verify if the user can't submit the form of Strategy and Consulting leaving the project in blank", () => {
        strategyAndConsulting.fillName(forms.randomFirstName)
        strategyAndConsulting.fillEmail(forms.randomEmail)
        strategyAndConsulting.chooseLocationOption()
        strategyAndConsulting.fillABoutProject('')
        strategyAndConsulting.submitForm()
        expect(messages.getProjectErrorMessage()).toContain(messages.projectMessage)
    })

    it("Should verify if the user can't submit the form of Strategy and Consulting leaving everything in blank", () => {
        strategyAndConsulting.submitForm()
        expect(messages.getNameErrorMessage()).toContain(messages.nameMessage)
        expect(messages.getEmailErrorMessage()).toContain(messages.emailMessage)
        expect(messages.getSelectionLocationErrorMessage()).toContain(messages.selectLocation)
        expect(messages.getProjectErrorMessage()).toContain(messages.projectMessage)
    })
})