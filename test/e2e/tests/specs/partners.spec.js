const PartnersPage = require('../pages/partners/partners.po')
const OraclePage = require('../pages/partners/oraclePage.po')
const Messages = require('../pages/components/messages')
const Forms = require('../testData/forms.td')


describe('I should test the behavior of partners page navigating between pages and submit the Oracle partner form', () => {

    const partnersPage = new PartnersPage()
    const oraclePage = new OraclePage()
    const messages = new Messages()
    const forms = new Forms()

    it('Should verify if the user can submit the oracle form of with success', () => {
        partnersPage.clickLinkPartners()
        partnersPage.scrollToAndClickOraclePartner()
        oraclePage.fillFirstName(forms.randomFirstName)
        oraclePage.fillLastName(forms.randomLastName)
        oraclePage.fillEmail(forms.randomEmail)
        oraclePage.fillCompany(forms.randomCompany)
        oraclePage.fillPhone(forms.randomPhone)
        oraclePage.clickButonSubmit()
        expect(messages.getSuccesMessage()).toContain(messages.formSubmitedSucessMessage)
    })
})

describe('I should test the behavior of partners page and try to submit the Oracle partner form leaving blank spaces', () => {

    const partnersPage = new PartnersPage()
    const oraclePage = new OraclePage()
    const messages = new Messages()
    const forms = new Forms()

    beforeEach(() => {
        browser.get(partnersPage.relativeUrl.concat(oraclePage.endOfRelativeUrl))
    })

    it("Should verify if the user can't submit the form leaving the name in blank", () => {
        oraclePage.fillFirstName('')
        oraclePage.fillLastName(forms.randomLastName)
        oraclePage.fillEmail(forms.randomEmail)
        oraclePage.fillCompany(forms.randomCompany)
        oraclePage.fillPhone(forms.randomPhone)
        oraclePage.clickButonSubmit()
        expect(messages.getNameErrorMessage()).toContain(messages.nameMessage)
    })

    it("Should verify if the user can't submit the form leaving the lastname in blank", () => {
        oraclePage.fillFirstName(forms.randomFirstName)
        oraclePage.fillLastName('')
        oraclePage.fillEmail(forms.randomEmail)
        oraclePage.fillCompany(forms.randomCompany)
        oraclePage.fillPhone(forms.randomPhone)
        oraclePage.clickButonSubmit()
        expect(messages.getLastNameErrorMessage()).toContain(messages.lastNameMessage)
    })

    it("Should verify if the user can't submit the form leaving the email in blank", () => {
        oraclePage.fillFirstName(forms.randomFirstName)
        oraclePage.fillLastName(forms.randomLastName)
        oraclePage.fillEmail('')
        oraclePage.fillCompany(forms.randomCompany)
        oraclePage.fillPhone(forms.randomPhone)
        oraclePage.clickButonSubmit()
        expect(messages.getEmailErrorMessage()).toContain(messages.emailMessage)
    })

    it("Should verify if the user can't submit the form leaving the company in blank", () => {
        oraclePage.fillFirstName(forms.randomFirstName)
        oraclePage.fillLastName(forms.randomLastName)
        oraclePage.fillEmail(forms.randomEmail)
        oraclePage.fillCompany('')
        oraclePage.fillPhone(forms.randomPhone)
        oraclePage.clickButonSubmit()
        expect(messages.getCompanyErrorMessage()).toContain(messages.companyMessage)
    })

    it("Should verify if the user can't submit the form leaving the phone in blank", () => {
        oraclePage.fillFirstName(forms.randomFirstName)
        oraclePage.fillLastName(forms.randomLastName)
        oraclePage.fillEmail(forms.randomEmail)
        oraclePage.fillCompany(forms.randomCompany)
        oraclePage.fillPhone('')
        oraclePage.clickButonSubmit()
        expect(messages.getPhoneErrorMessage()).toContain(messages.phoneMessage)
    })

    it("Should verify if the user can't submit the form leaving everything in blank", () => {
        oraclePage.clickButonSubmit()
        expect(messages.getNameErrorMessage()).toContain(messages.nameMessage)
        expect(messages.getLastNameErrorMessage()).toContain(messages.lastNameMessage)
        expect(messages.getEmailErrorMessage()).toContain(messages.emailMessage)
        expect(messages.getCompanyErrorMessage()).toContain(messages.companyMessage)
        expect(messages.getPhoneErrorMessage()).toContain(messages.phoneMessage)
    })
})
