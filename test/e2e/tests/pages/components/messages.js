const protractorHelper = require('protractor-helper')

class Messages {
    constructor() {
        this.nameMessage = 'Por favor, preencha seu nome.'
        this.lastNameMessage = 'Por favor, preencha seu sobrenome.'
        this.emailMessage = 'Insira um email válido'
        this.companyMessage = 'Por favor, preencha o nome da empresa.'
        this.phoneMessage = 'Insira um telefone válido'
        this.formSubmitedSucessMessage = 'Formulário enviado com sucesso!'
        this.selectLocation = 'Selecione um ítem'
        this.projectMessage = 'Por favor, preencha este campo.'
    }

    getSuccesMessage() {
        const succesMessage = element(by.id('success-message'))
        protractorHelper.waitForElementVisibility(succesMessage, 150000)
        return succesMessage.getText()
    }

    getNameErrorMessage() {
        const nameErrorMessage = element(by.id('firstname-error-message'))
        protractorHelper.waitForElementVisibility(nameErrorMessage, 150000)
        return nameErrorMessage.getText()
    }

    getLastNameErrorMessage() {
        const lastNameErrorMessage = element(by.id('lastname-error-message'))
        protractorHelper.waitForElementVisibility(lastNameErrorMessage, 150000)
        return lastNameErrorMessage.getText()
    }

    getEmailErrorMessage() {
        const emailErrorMessage = element(by.id('email-error-message'))
        protractorHelper.waitForElementVisibility(emailErrorMessage, 150000)
        return emailErrorMessage.getText()
    }

    getCompanyErrorMessage() {
        const companyErrorMessage = element(by.id('company-error-message'))
        protractorHelper.waitForElementVisibility(companyErrorMessage, 150000)
        return companyErrorMessage.getText()
    }

    getPhoneErrorMessage() {
        const phoneErrorMessage = element(by.id('phone-error-message'))
        protractorHelper.waitForElementVisibility(phoneErrorMessage, 150000)
        return phoneErrorMessage.getText()
    }

    getProjectErrorMessage() {
        const projectErrorMessage = element(by.id('project_info_error'))
        protractorHelper.waitForElementVisibility(projectErrorMessage, 150000)
        return projectErrorMessage.getText()
    }

    getSelectionLocationErrorMessage() {
        const locationErrorMessage = element(by.id('select_location_error'))
        protractorHelper.waitForElementVisibility(locationErrorMessage, 150000)
        return locationErrorMessage.getText()
    }
}

module.exports = Messages;