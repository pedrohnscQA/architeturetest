const protractorHelper = require('protractor-helper')

class strategyAndConsulting {
    constructor() {
        this.endOfRelativeUrl = '/strategy-and-consulting'
        this.inputFirstName = element(by.id('firstname'))
        this.inputEmail = element(by.id('email'))
        this.selectLocation = element(by.id('select_a_location'))
        this.option = element(by.id('country1'))
        this.inputAboutProject = element(by.id('tell_us_about_your_project'))
        this.buttonSubmit = element(by.id('what-we-do-form'))
    }

    fillName(name) {
        protractorHelper.fillFieldWithTextWhenVisible(this.inputFirstName, name)
    }

    fillEmail(email) {
        protractorHelper.fillFieldWithTextWhenVisible(this.inputEmail, email)
    }

    chooseLocationOption() {
        protractorHelper.clickWhenClickable(this.selectLocation, 170000)
        protractorHelper.clickWhenClickable(this.option, 170000)
    }

    fillABoutProject(project) {
        protractorHelper.fillFieldWithTextWhenVisible(this.inputAboutProject, project, 170000)
    }

    submitForm() {
        protractorHelper.clickWhenClickable(this.buttonSubmit, 170000)
    }
}

module.exports = strategyAndConsulting;