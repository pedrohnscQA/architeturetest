const protractorHelper = require('protractor-helper')

class whatWeDo {
    constructor() {
        this.relativeUrl = '/what-we-do'
        this.strategyAndConsulting = element(by.id('page0'))
        this.linkWhatWeDo = element.all(by.linkText(('O Que Fazemos'))).get(0)
    }

    clickLinkWhatWeDo() {
        protractorHelper.waitForElementVisibility(this.linkWhatWeDo, 170000)
        protractorHelper.clickWhenClickable(this.linkWhatWeDo, 170000)
    }

    clickInTheBox() {
        protractorHelper.waitForElementVisibility(this.strategyAndConsulting, 170000)
        protractorHelper.clickWhenClickable(this.strategyAndConsulting, 170000)
    }

}

module.exports = whatWeDo;