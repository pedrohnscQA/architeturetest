const protractorHelper = require('protractor-helper')

class oraclePage {
    constructor() {
        this.endOfRelativeUrl = '/oracle-implementation-services'
        this.inputFirstName = element(by.id('firstname'))
        this.inputLastName = element(by.id('lastname'))
        this.imputEmail = element(by.id('email'))
        this.inputCompany = element(by.id('company'))
        this.inputPhone = element(by.id('phone'))
        this.buttonSubmitForm = element(by.id('partners-form'))
    }

    fillFirstName(name) {
        protractorHelper.waitForElementVisibility(this.inputFirstName, 150000)
        protractorHelper.fillFieldWithTextWhenVisible(this.inputFirstName, name)
    }

    fillLastName(lastname) {
        protractorHelper.waitForElementVisibility(this.inputLastName, 150000)
        protractorHelper.fillFieldWithTextWhenVisible(this.inputLastName, lastname)
    }

    fillEmail(email) {
        protractorHelper.waitForElementVisibility(this.imputEmail, 150000)
        protractorHelper.fillFieldWithTextWhenVisible(this.imputEmail, email)
    }

    fillCompany(company) {
        protractorHelper.waitForElementVisibility(this.inputCompany, 150000)
        protractorHelper.fillFieldWithTextWhenVisible(this.inputCompany, company)
    }

    fillPhone(phone) {
        protractorHelper.waitForElementVisibility(this.inputPhone, 150000)
        protractorHelper.fillFieldWithTextWhenVisible(this.inputPhone, phone)
    }

    clickButonSubmit() {
        protractorHelper.waitForElementVisibility(this.buttonSubmitForm, 150000)
        protractorHelper.clickWhenClickable(this.buttonSubmitForm, 150000)
    }

}

module.exports = oraclePage;